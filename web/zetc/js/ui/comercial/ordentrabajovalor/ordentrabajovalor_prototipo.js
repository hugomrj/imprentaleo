
var data_valores = [];          
var data_valores_id = 0;   
    



function  OrdenTrabajoValor(){
    
    this.tipo = "ordentrabajovalor";       
    
    this.campoid =  'id';
        this.tablacampos =  ['tam_largo', 'tam_ancho', 'corte_largo', 'corte_ancho' ,
        'descripcion', 'pliegos'
        ];   
    
    this.tablaformat =  [ 'N', 'N', 'N', 'N', 'C', 'N' ];   
    
    
    
}




OrdenTrabajoValor.prototype.new = function( obj  ) {                
  
  
    ajax.url = html.url.absolute()+'/comercial/ordentrabajovalores/htmf/form.html'; 
    ajax.metodo = "GET";  
    modal.ancho = 900;
    var obj = modal.ventana.mostrar("ind");       

  
  
    CalculadoraDeCortes();
  

    
    var obj_val = new OrdenTrabajoValor();
    obj_val.form_ini();
    obj_val.form_acciones(obj_val);
            
       
    // calculos
    var ordentrabajovalores_cantidad_pedido = document.getElementById('ordentrabajovalores_cantidad_pedido');
    ordentrabajovalores_cantidad_pedido.onblur = function() {
    
        
        ordentrabajovalores_cantidad_pedido.value = fmtNum(ordentrabajovalores_cantidad_pedido.value);

        // aca hay que hacer el calculo                    
        //var otval_cantidad_pedido = document.getElementById('otval_cantidad_pedido');
        
        var ordentrabajovalores_pliegos = document.getElementById('ordentrabajovalores_pliegos');
        var ordentrabajovalores_sale = document.getElementById('ordentrabajovalores_sale');
        
        var parte_entera = Math.trunc( NumQP(ordentrabajovalores_cantidad_pedido.value) / 
                NumQP(ordentrabajovalores_sale.value)) ;
        
        var parte_completa = (NumQP(ordentrabajovalores_cantidad_pedido.value) / 
                NumQP(ordentrabajovalores_sale.value)) ;

        if (parte_completa > parte_entera){
            ordentrabajovalores_pliegos.value = parte_entera + 1;
        }
        else{
            ordentrabajovalores_pliegos.value = parte_entera ;
        }

    
    
    }    
       
       
       
       
    
    boton.ini(obj_val);
    document.getElementById( obj_val.tipo +'-acciones' ).innerHTML 
                        =  boton.basicform.get_botton_add();        
  
  


    var btn_ordentrabajovalor_cancelar = document.getElementById('btn_ordentrabajovalor_cancelar');
    btn_ordentrabajovalor_cancelar.addEventListener('click',
        function(event) {    

            var ven = document.getElementById('vntind');
            modal.ventana.cerrar(ven);     
            
        },
        false
    );    
 
 
 
 
 
    var btn_ordentrabajovalor_guardar = document.getElementById('btn_ordentrabajovalor_guardar');
    btn_ordentrabajovalor_guardar.addEventListener('click',
        function(event) {    

            

            var det = new OrdenTrabajoValor();
            
            if ( det.form_validar()) {
                
                
                data_valores_id++;
                
                document.getElementById('ordentrabajovalores_id').value = data_valores_id;

        

                form.name = "form_ordentrabajovalores";               
                var json_det =  form.datos.getjson() ;              
                
           
                
                var odet = JSON.parse(json_det);

                data_valores.push(odet);            
                
                ordentrabajo_valores_tabla  ( JSON.stringify(data_valores) );     
                
                btn_ordentrabajovalor_cancelar.click();
                
                ordentrabajo_valores_tabla_registro();
  
                
            }            

            
        },
        false
    );    
 
  
 
  
};




OrdenTrabajoValor.prototype.form_ini = function(obj) {    
    
}


OrdenTrabajoValor.prototype.form_acciones = function(obj) {   
    
    
    
    
}







OrdenTrabajoValor.prototype.form_validar = function() {   

    
    var ordentrabajovalores_sale = document.getElementById('ordentrabajovalores_sale');    
    if (ordentrabajovalores_sale.value <= 0)         
    {
        msg.error.mostrar("Error en cantidad que sale por pliego");           
        ordentrabajovalores_sale.focus();
        ordentrabajovalores_sale.select();        
        return false;
    }       




    var ordentrabajovalores_cantidad_pedido = document.getElementById('ordentrabajovalores_cantidad_pedido');    
    if (ordentrabajovalores_cantidad_pedido.value <= 0)         
    {
        msg.error.mostrar("Error en cantidad pedido");           
        ordentrabajovalores_cantidad_pedido.focus();
        ordentrabajovalores_cantidad_pedido.select();        
        return false;
    }       





    return true;
};





OrdenTrabajoValor.prototype.sublista = function(json) {    
            
    var ojson = JSON.parse(form.json) ; 
    
    var det = ojson['valores'];

    var sub = new OrdenTrabajoValor();


    tabla.json = JSON.stringify(det);
    tabla.ini(sub);
    tabla.id = "valores-tabla";
    tabla.tbody_id = "valores-tb"
    tabla.gene();   
    tabla.formato(sub);

    
}

