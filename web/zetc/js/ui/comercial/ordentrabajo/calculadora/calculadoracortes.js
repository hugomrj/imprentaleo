
var maximo_pliego = 0;

function Calculadora_cortes ( ){


        
        var maximo_pliego_valores = document.getElementById('planopliego');    
        maximo_pliego = maximo_pliego_valores.clientWidth;

    
            var calc_tam_ancho = document.getElementById('calc_tam_ancho');
            calc_tam_ancho.value = "100";
            calc_tam_ancho.onblur = function() {
                Generador_cortes();
                document.getElementById('original').checked = true;
            }
            
            
    
            var calc_tam_largo = document.getElementById('calc_tam_largo');
            calc_tam_largo.value = "100";
            calc_tam_largo.onblur = function() {
                Generador_cortes();
                document.getElementById('original').checked = true;
            }            
    
        
        
    
            var calc_corte_ancho = document.getElementById('calc_corte_ancho');
            calc_corte_ancho.onblur = function() {
                
                calc_corte_ancho.value  = validaFloat(calc_corte_ancho.value);                   
                Generador_cortes();
                
                document.getElementById('original').checked = true;
            }            
    
    
            var calc_corte_largo = document.getElementById('calc_corte_largo');
            calc_corte_largo.onblur = function() {
                
                calc_corte_largo.value  = validaFloat(calc_corte_largo.value);                                                   
                Generador_cortes();                                
                document.getElementById('original').checked = true;
                
                
            }            
        
    
            
            Generador_cortes();
            
            var original = document.getElementById('original');      
            original.addEventListener('click',
                function(event) {
                    original_inverfido();                    
                },
                false
            );                

            var invertido = document.getElementById('invertido');      
            invertido.addEventListener('click',
                function(event) {
                    original_inverfido();                    
                },
                false
            );                



            // botones acciones
            
            boton.objeto = "calculadora";
            document.getElementById( 'calculadora-acciones' ).innerHTML 
                        =  boton.basicform.get_botton_okcan();              
            
            



            var btn_calculadora_cancelar = document.getElementById('btn_calculadora_cancelar');
            btn_calculadora_cancelar.addEventListener('click',
                function(event) {    

                    var ven = document.getElementById('vntcalc');
                    modal.ventana.cerrar(ven);     

                },
                false
            );    




            var btn_calculadora_aceptar = document.getElementById('btn_calculadora_aceptar');
            btn_calculadora_aceptar.addEventListener('click',
                function(event) {    


                    var calc_corte_ancho = document.getElementById('calc_corte_ancho');
                    var calc_corte_largo = document.getElementById('calc_corte_largo');
                    
                    if (( Number(calc_corte_ancho.value) <= 0 )  || ( Number(calc_corte_largo.value) <= 0 ))  {
                        return;
                    }                        

                    var calc_tam_ancho = document.getElementById('calc_tam_ancho');
                    var calc_tam_largo = document.getElementById('calc_tam_largo');
                    
                    if (( Number(calc_tam_ancho.value) <= 0 )  || ( Number(calc_tam_largo.value) <= 0 ))  {
                        return;
                    }                        


                    var corte_ancho = document.getElementById('ordentrabajovalores_corte_ancho');
                    corte_ancho.value = calc_corte_ancho.value;

                    var corte_largo = document.getElementById('ordentrabajovalores_corte_largo');
                    corte_largo.value = calc_corte_largo.value;                 
                 
                    var tam_ancho = document.getElementById('ordentrabajovalores_tam_ancho');
                    tam_ancho.value = calc_tam_ancho.value;

                    var tam_largo = document.getElementById('ordentrabajovalores_tam_largo');
                    tam_largo.value = calc_tam_largo.value;



                    var ordentrabajovalores_pliegos = document.getElementById('ordentrabajovalores_pliegos');
                    //var otval_pliegos = document.getElementById('otval_pliegos');
                    var ordentrabajovalores_sale = document.getElementById('ordentrabajovalores_sale');
                    ordentrabajovalores_sale.value = numeros_pliegos.innerHTML;
                    

                    
                    document.getElementById('ordentrabajovalores_cantidad_pedido').focus();
                    document.getElementById('ordentrabajovalores_cantidad_pedido').select();

                    
                    btn_calculadora_cancelar.click();


                },
                false
            );    








 

}








function Generador_cortes ( ){
    
   var maximo_pliego = 600;   
    
    var planopliego = document.getElementById('planopliego');       
    planopliego.style.width = maximo_pliego + "px";
    planopliego.style.height = maximo_pliego + "px";      
   
   // alert(600)
   
   
    var corte_ancho = document.getElementById('calc_corte_ancho');    
    var corte_largo = document.getElementById('calc_corte_largo');    
    

    if ( Number(corte_ancho.value) <= 0 ){
        return;
    }

    if ( Number(corte_largo.value) <= 0 ){
        return;
    }
    
    var pliego_ancho = document.getElementById('calc_tam_ancho');    
    var pliego_largo = document.getElementById('calc_tam_largo');    
    
    
        

    // si largo es ancho es mayor
    if ( Number(pliego_ancho.value) > Number(pliego_largo.value) ) {   
        
        planopliego.style.width = maximo_pliego + "px";
        var porcentaj = (  pliego_largo.value * 100) / pliego_ancho.value;        
        planopliego.style.height = (maximo_pliego * porcentaj ) / 100 + "px" ;
    }
                
    
    if ( Number(pliego_ancho.value) < Number(pliego_largo.value) ) {

        planopliego.style.height = maximo_pliego + "px";
        var porcentaj = (  pliego_ancho.value * 100) / pliego_largo.value;  
        planopliego.style.width = (maximo_pliego * porcentaj ) / 100 + "px" ;        
        
    }


    if ( Number(pliego_ancho.value) == Number(pliego_largo.value) ) {
      planopliego.style.width = maximo_pliego + "px";
      planopliego.style.height = maximo_pliego + "px";
    }
   
        
    //eliminar todos los hijos de un div
    var element = document.getElementById("planopliego");
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
  
    
    


    // obtener pixeles reales de cortes
    
    var corte_ancho_px = 0;        
    corte_ancho_px = ( corte_ancho.value * planopliego.clientWidth ) / pliego_ancho.value ;
    corte_ancho_px = corte_ancho_px -1 ;
    
    
    
    var corte_largo_px = 0;        
    corte_largo_px = ( corte_largo.value * planopliego.clientHeight ) / pliego_largo.value ;
    corte_largo_px = corte_largo_px -1 ;


    
console.log("corte_largo_px : " + corte_largo_px)        



    /*
    var minimopx = 0;
    minimopx = corte_ancho_px;
    if (corte_ancho_px > corte_largo_px){
        minimopx = corte_largo_px;
    }
    minimopx = minimopx /2;
    */
    
    var pliego_ancho_resto = 0;
    var pliego_largo_resto = 0;
    var corte_left = -1;
    var corte_top = -1;
    
    // inicio 
    var contador = 0;
    
    
    //pliego_ancho_resto = Number(pliego_ancho.value);
    pliego_largo_resto = Number(pliego_largo.value);

        var corte_top = 0;
        while ( pliego_largo_resto >= Number(corte_largo.value) ) {
            
            var corte_left = 0;
            pliego_ancho_resto = Number(pliego_ancho.value);
            while ( pliego_ancho_resto >= Number(corte_ancho.value)   ) {

                    var divC = document.createElement('div');    
                    divC.className = "corte";    
                    divC.style.width = corte_ancho_px +"px";
                    divC.style.height = corte_largo_px +"px";



                    divC.style.left = corte_left +"px";
                    divC.style.top = corte_top +"px";;
                           
                    corte_left = corte_left +corte_ancho_px;
                    corte_left = corte_left + 1;
                    
                    contador ++;
                    //divC.innerHTML=  '<p style="font-size: ' + (minimopx) + 'px; ">' + contador + '</p>' ;
                    divC.innerHTML=  '<p>' + contador + '</p>' ;
                    
                    
                    planopliego.appendChild(divC);        


                    pliego_ancho_resto = pliego_ancho_resto - Number(corte_ancho.value) ;   
            }    
            
            corte_top = corte_top + corte_largo_px;
            corte_top = corte_top + 1;
            
            pliego_largo_resto = pliego_largo_resto - Number(corte_largo.value) ;   
            
        }


        // si sobra de ancho

        if ( pliego_ancho_resto >= Number(corte_largo.value) )
        {            

            while ( pliego_ancho_resto >=  Number(corte_largo.value) ) {            

                var corte_top = 0;
                pliego_largo_resto = Number(pliego_largo.value);            
                while ( pliego_largo_resto >= Number(corte_ancho.value)   ) {

                        var divC = document.createElement('div');    
                        divC.className = "corte";    

                        divC.style.width = corte_largo_px +"px";;
                        divC.style.height = corte_ancho_px +"px";;

                        divC.style.left = corte_left +"px";;
                        divC.style.top = corte_top +"px";;

                        corte_top = corte_top + corte_ancho_px;
                        corte_top = corte_top +1 

                        contador ++;
                        divC.innerHTML=  '<p>' + contador + '</p>' ;
                        planopliego.appendChild(divC);        

                        pliego_largo_resto = pliego_largo_resto - Number(corte_ancho.value) ;   
                }                          

                corte_left = corte_left + corte_largo_px;
                corte_left = corte_left + 1;
            
                pliego_ancho_resto = pliego_ancho_resto - Number(corte_largo.value) ;   
        
            }
        }

     
     
     
        // si sobra de largo

        if ( pliego_largo_resto >= Number(corte_ancho.value) )
        {            

            while ( pliego_largo_resto >=  Number(corte_ancho.value) ) {            
                
                pliego_ancho_resto = Number(pliego_ancho.value);     
                
                var corte_left = 0;    
                while ( pliego_ancho_resto >= Number(corte_largo.value)   ) {

                        var divC = document.createElement('div');    
                        divC.className = "corte";    

                        divC.style.width = corte_largo_px +"px";;
                        divC.style.height = corte_ancho_px +"px";;

                        divC.style.left = corte_left +"px";;
                        divC.style.top = corte_top +"px";;
                        
                        corte_left = corte_left + corte_largo_px;
                        corte_left = corte_left + 1;                        

                        contador ++;
                        divC.innerHTML=  '<p>' + contador + '</p>' ;
                        planopliego.appendChild(divC);        

                        pliego_ancho_resto = pliego_ancho_resto - Number(corte_largo.value) ;   
                }                          

                corte_top = corte_top + corte_ancho_px;
                corte_top = corte_top +1 

                pliego_largo_resto = pliego_largo_resto - Number(corte_ancho.value) ;   

            }
        }
     
     
     
     // etiquetas
     
     
         var numeros_pliegos = document.getElementById("numeros_pliegos");
         numeros_pliegos.innerHTML = contador;
     
        var papelutilizado = document.getElementById("papelutilizado");
        papelutilizado.innerHTML =  parseFloat(
                        (100 * ( corte_ancho.value * corte_largo.value ) * contador ) / 
                        (pliego_ancho.value * pliego_largo.value) 
                    ).toFixed(2) ;
     
        var desperdiciado = document.getElementById("desperdiciado");
        desperdiciado.innerHTML = parseFloat( 100 - papelutilizado.innerHTML  ).toFixed(2)
          
          

          
          
    // verficar si sobra espacio para seguir cortando

    //alert(pliego_largo_resto);



    /*
pliego_valores.style.width = "600px";    
pliego_valores.style.height = "600px";    
*/

    
}


function original_inverfido ( ){    
    
    
    var c_ancho = document.getElementById("calc_corte_ancho").value;
    var c_largo = document.getElementById("calc_corte_largo").value;
        
    document.getElementById("calc_corte_ancho").value = c_largo;
    document.getElementById("calc_corte_largo").value = c_ancho;
        
    Generador_cortes();    
    
    
}






function CalculadoraDeCortes ( ){    
    
    
    
    
    
    
    
    var irCalculadora = document.getElementById('irCalculadora');   
    irCalculadora.addEventListener('click',
        function()
        {            
            //VentanaModal("4",  '../Calculadora/jspf/cortes.jspx', 900 );            
                             
            
            //ajax.url = html.url.absolute()+'/comercial/ordentrabajovalores/htmf/cortes.jspx'; 
            ajax.url = html.url.absolute()+'/comercial/ordentrabajovalores/htmf/calculadora.html'; 
            ajax.metodo = "GET";  
            modal.ancho = 800;
            var obj = modal.ventana.mostrar("calc");       

            
            Calculadora_cortes();      
            
            
            document.getElementById('calc_tam_ancho').focus();
            document.getElementById('calc_tam_ancho').select();                         
            
        },
        false
    );
    
    

    
    
}



/// funciones




function validaFloat(valor) {
    var RE =/^[0-9]+([.])?([0-9]+)?$/;
    if (RE.test(valor)) {
        return valor;
    } else {
        return 0;
    }
}




















