

var datadet = [];          
var datadet_id = 0;   
    




function OrdenTrabajoDetalle(){
    
    this.tipo = "ordentrabajodetalle";       
    
    this.campoid =  'id';
    this.tablacampos =  ['cantidad', 'cantidad_hoja', 'descripcion', 
        'precio_unitario', 'porcentaje0',  'porcentaje5', 
        'porcentaje10'
        ];   
   
    this.tablaformat =  [ 'N', 'N', 'C', 
        'N', 'N', 'N',
        'N' ];   
    
    
    
}






OrdenTrabajoDetalle.prototype.new = function( obj  ) {                
  
    var det = new OrdenTrabajoDetalle();
    det.form_ini();
    det.form_acciones(det);
            
    
   
  
  
    var detalle_cantidad_hoja = document.getElementById('detalle_cantidad_hoja');             
    detalle_cantidad_hoja.focus();
    detalle_cantidad_hoja.select();    

    
    
    boton.ini(obj);
    document.getElementById( obj.tipo +'-acciones' ).innerHTML 
                        =  boton.basicform.get_botton_add();        
  
  
   
  
  
  
  
    var btn_otd_guardar = document.getElementById('btn_ordentrabajodetalle_guardar');
    btn_otd_guardar.addEventListener('click',
        function(event) {               

            var det = new OrdenTrabajoDetalle();
            
            if ( det.form_validar()) {
                
                
                datadet_id++;
                
                document.getElementById('detalle_factura_detalle').value = datadet_id;


                // cargar detalle_impuesto desde detalle_impuesto_sel
                var select = document.getElementById('detalle_impuesto_sel');
                var sel_value = select.options[select.selectedIndex].value;    

                var impuesto = document.getElementById('detalle_impuesto');
                impuesto.value  = parseInt(sel_value);

                //enviar valores a cuadros correspondientes
                // porcentaje 0 5 10


                var porcentaje0 = document.getElementById('detalle_porcentaje0');            
                var porcentaje5 = document.getElementById('detalle_porcentaje5');
                var porcentaje10 = document.getElementById('detalle_porcentaje10');

                var subtotal = document.getElementById('detalle_sub_total');

                porcentaje0.value = 0;
                porcentaje5.value = 0;
                porcentaje10.value = 0;


                if (impuesto.value == 0){
                    porcentaje0.value = subtotal.value;
                }            
                else if (impuesto.value == 5){
                    porcentaje5.value = subtotal.value;
                }                        
                else if (impuesto.value == 10){
                    porcentaje10.value = subtotal.value;
                }            

                form.name = "form_ordentrabajodetalle";               
                var json_det =  form.datos.getjson() ;      
                
                var odet = JSON.parse(json_det);

                datadet.push(odet);            

                ordentrabajo_detalle_tabla  ( JSON.stringify(datadet) );            

                var ven = document.getElementById('vntind');
                modal.ventana.cerrar(ven);                 

                ordentrabajo_detalle_tabla_registro();
                
                
            }            
            

        },
        false
    )







    var btn_otd_cancelar = document.getElementById('btn_ordentrabajodetalle_cancelar');
    btn_otd_cancelar.addEventListener('click',
        function(event) {    

            var ven = document.getElementById('vntind');
            modal.ventana.cerrar(ven);     
            
        },
        false
    );    
 
  
};








OrdenTrabajoDetalle.prototype.modal_reg = function( obj, linea_id  ) {                
  
  
    var det = new OrdenTrabajoDetalle();
    det.form_ini();
    det.form_acciones(det);
 
    
    //ff = datadet.filter("factura_detalle = " + linea_id )   ;
    let arrayFilter = datadet.filter(e => e.factura_detalle == linea_id)

    
    // mostrar en formulario
    this.modal_reg_datos(arrayFilter);
    
    
    boton.ini(obj);
    document.getElementById( obj.tipo +'-acciones' ).innerHTML 
                        =  boton.basicform.get_botton_del();        
  
  
    var btn_otd_eliminar = document.getElementById('btn_ordentrabajodetalle_eliminar');
    btn_otd_eliminar.addEventListener('click',
        function(event) {               
            
            var indice_id = ordentrabajo_detalle_obtener_indice(linea_id);            
            
            ordentrabajo_detalle_tabla_borrar_linea(indice_id);
            
            
            var ven = document.getElementById('vntind');
            modal.ventana.cerrar(ven);   

        },
        false
    )







    var btn_otd_cancelar = document.getElementById('btn_ordentrabajodetalle_cancelar');
    btn_otd_cancelar.addEventListener('click',
        function(event) {    

            var ven = document.getElementById('vntind');
            modal.ventana.cerrar(ven);     
            
        },
        false
    );    
 
  
};









OrdenTrabajoDetalle.prototype.modal_reg_datos = function( arrayFilter  ) {                
  
  
    document.getElementById("detalle_cantidad_hoja").value = arrayFilter[0]['cantidad_hoja'];
    document.getElementById("detalle_descripcion").value = arrayFilter[0]['descripcion'];
    document.getElementById("detalle_cantidad").value = arrayFilter[0]['cantidad'];
    document.getElementById("detalle_precio_unitario").value = arrayFilter[0]['precio_unitario'];
    document.getElementById("detalle_sub_total").value = arrayFilter[0]['sub_total'];

    document.getElementById("detalle_factura_detalle").value = arrayFilter[0]['factura_detalle'];
  
  
    var imp = arrayFilter[0]['impuesto_sel']['impuesto_sel'];
   
  
    var select = document.getElementById('detalle_impuesto_sel');
  
  
    for (let i = select.options.length; i >= 0; i--) {
        select.remove(i);
    }
    
  
    if (imp == 10){      
        var opt = document.createElement('option');
        opt.value = 10;
        opt.innerHTML = "10%";
        select.appendChild(opt);            
    }
  
  
    if (imp == 5){      
        var opt = document.createElement('option');
        opt.value = 5;
        opt.innerHTML = "5%";
        select.appendChild(opt);            
    }
  
    if (imp == 0){      
        var opt = document.createElement('option');
        opt.value = 0;
        opt.innerHTML = "exenta";
        select.appendChild(opt);            
    }

  
    this.modal_reg_enabled(true);
    
};




OrdenTrabajoDetalle.prototype.modal_reg_enabled = function( bool  ) {                
  
  document.getElementById('detalle_cantidad_hoja').disabled =  bool;
  document.getElementById('detalle_descripcion').disabled =  bool;
  document.getElementById('detalle_cantidad').disabled =  bool;
  document.getElementById('detalle_precio_unitario').disabled =  bool;
  document.getElementById('detalle_sub_total').disabled =  bool;
  document.getElementById('detalle_impuesto_sel').disabled =  bool;
  
  document.getElementById('detalle_impuesto').style.visibility = 'hidden';
      
};






OrdenTrabajoDetalle.prototype.form_ini = function() {    
        
    document.getElementById("detalle_sub_total").disabled = true;
        
    var detalle_cantidad_hoja = document.getElementById('detalle_cantidad_hoja');   
    detalle_cantidad_hoja.onblur = function() {
        detalle_cantidad_hoja.value = fmtNum(detalle_cantidad_hoja.value);
    }        
    
    
    var detalle_cantidad = document.getElementById('detalle_cantidad');   
    detalle_cantidad.onblur = function() {
        detalle_cantidad.value = fmtNum(detalle_cantidad.value);
    }        
    
        
    var detalle_precio_unitario = document.getElementById('detalle_precio_unitario');   
    detalle_precio_unitario.onblur = function() {
        detalle_precio_unitario.value = fmtNum(detalle_precio_unitario.value);
    }        
            
};






OrdenTrabajoDetalle.prototype.form_acciones = function(obj) {    
    
    
    var detalle_sub_total = document.getElementById('detalle_sub_total');   
    var detalle_cantidad = document.getElementById('detalle_cantidad');   
    var detalle_precio_unitario = document.getElementById('detalle_precio_unitario');   
    
        
        
    detalle_cantidad.onblur = function() {               
        detalle_sub_total.value 
                = fmtNum( NumQP(detalle_cantidad.value) * NumQP(detalle_precio_unitario.value));
        
        detalle_cantidad.value = fmtNum(detalle_cantidad.value);     
    }    
        
        
        
    detalle_precio_unitario.onblur = function() {                
        detalle_sub_total.value 
                = fmtNum( NumQP(detalle_cantidad.value) * NumQP(detalle_precio_unitario.value));
        
        detalle_precio_unitario.value = fmtNum(detalle_precio_unitario.value);
    }    
    
};







OrdenTrabajoDetalle.prototype.form_validar = function() {   
    

    var detalle_cantidad_hoja = document.getElementById('detalle_cantidad_hoja');    
    if (detalle_cantidad_hoja.value <= 0)         
    {
        msg.error.mostrar("Cantidad hojas vacia");           
        detalle_cantidad_hoja.focus();
        detalle_cantidad_hoja.select();        
        return false;
    }       



    var detalle_descripcion = document.getElementById('detalle_descripcion');    
    if (detalle_descripcion.value == "")         
    {
        msg.error.mostrar("Descripcion vacia");           
        detalle_descripcion.focus();
        detalle_descripcion.select();        
        return false;
    }   



    var detalle_cantidad = document.getElementById('detalle_cantidad');    
    if (detalle_cantidad.value <= 0)         
    {
        msg.error.mostrar("Cantidad vacia");           
        detalle_cantidad.focus();
        detalle_cantidad.select();        
        return false;
    }       




    var detalle_precio_unitario = document.getElementById('detalle_precio_unitario');    
    if (detalle_precio_unitario.value <= 0)         
    {
        msg.error.mostrar("Precio incorrecto");           
        detalle_precio_unitario.focus();
        detalle_precio_unitario.select();        
        return false;
    }  


    return true;
};




OrdenTrabajoDetalle.prototype.sublista = function(json) {    
            
    var ojson = JSON.parse(form.json) ; 
    
    var det = ojson['detalles'];

    var sub = new OrdenTrabajoDetalle();


    tabla.json = JSON.stringify(det);
    tabla.ini(sub);
    tabla.id = "detalle-tabla";
    tabla.tbody_id = "detalle-tb"
    tabla.gene();   
    tabla.formato(sub);

    
}

