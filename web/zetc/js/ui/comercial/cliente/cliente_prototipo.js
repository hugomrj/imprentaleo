

function Cliente(){
    
   this.tipo = "cliente";   
   this.recurso = "clientes";   
   this.value = 0;
   this.from_descrip = "";
   this.json_descrip = ['', ''];
   
   this.dom="";
   this.carpeta=  "/comercial";   
   
   this.titulosin = "Cliente";
   this.tituloplu = "Clientes";      
    
   this.campoid=  'cliente';
   this.tablacampos =  ['cliente', 'cedula', 'nombre', 'apellido', 
                                'direccion', 'telefono'];   
                            
   this.etiquetas =   ['Cliente', 'Cedula', 'Nombre', 'Apellido', 
                                'Direccion', 'Telefono'];   
   
   
   this.tablaformat =  [ 'N', 'N', 'C', 'C', 
                                'C', 'C' ];   
   
      
   this.tbody_id = "cliente-tb";
      
   this.botones_lista = [ this.lista_new] ;
   this.botones_form = "cliente-acciones";   
      
   this.tabs =  [];
   this.parent = null;
   

   
}





Cliente.prototype.lista_new = function( obj  ) {                
   
    reflex.form_new( obj );    
//    reflex.datos.combo(obj);    
    reflex.acciones.button_add( obj );                     

};





Cliente.prototype.form_validar = function() {   
    

    var cliente_nombre = document.getElementById('cliente_nombre');    
    if (cliente_nombre.value == "")         
    {
        msg.error.mostrar("Campo de nombre vacio");           
        cliente_nombre.focus();
        cliente_nombre.select();        
        return false;
    }       
    
    return true;
};











Cliente.prototype.form_ini = function() {    
    

    var cliente_cedula = document.getElementById('cliente_cedula');          
    cliente_cedula.onblur  = function() {           
        cliente_cedula.value = fmtNum(cliente_cedula.value);                                    
    }    
    cliente_cedula.onblur();

    
};






