
function Factura(){
    
   this.tipo = "factura";   
   this.recurso = "facturas";   
   this.value = 0;
   this.from_descrip = "";
   this.json_descrip = ['', ''];
   
   this.dom="";
   this.carpeta=  "/comercial";   
   
   this.titulosin = "Factura";
   this.tituloplu = "Facturas";      
    
    
   this.campoid=  'factura';
   
   this.tablacampos =  [ 'factura', 'fecha_factura', 'cliente.cedula' 
            , 'cliente.nombre', 'cliente.apellido', 'monto_total' ];   
   
    
   this.etiquetas =   [ 'numero_factura', 'fecha_factura', 'cliente.cedula' 
            , 'nombre', 'apellido', 'monto_total' ];   
   
   
   this.tablaformat =  [ 'N', 'D', 'N',
                        'C', 'C', 'N' ];   
   
      
   this.tbody_id = "factura-tb";
      
   this.botones_lista = [ this.new] ;
   this.botones_form = "factura-acciones";   
      
   this.tabs =  [];
   this.parent = null;
   
}




Factura.prototype.new = function( obj  ) {                
  
    factura_formbasico( obj );
    obj.form_ini(obj);



    //factura_acciones_add( obj );
    factura_acciones_guardar(obj);
  

  
};





Factura.prototype.form_validar = function() {   


    
    var factura_fecha_factura = document.getElementById('factura_fecha_factura');
    if (factura_fecha_factura.value == ""){
        msg.error.mostrar("Falta fecha");                    
        factura_fecha_factura.focus();
        factura_fecha_factura.select();                                       
        return false;        
    }  
  

    
    // Obtener el tbody por su id
    var tbody = document.getElementById("detalle-tb");
    // Verificar si el tbody tiene al menos una fila
    if (tbody && tbody.rows.length > 0) {
        //alert("La tabla tiene elementos.");
    } else {
        msg.error.mostrar("No existen registros para crear la factura");                    
        return false;        
    }    
    
    
    return true;
};











Factura.prototype.form_ini = function(obj) {    


    var obj = new Factura();


    ajax.url = html.url.absolute() + obj.carpeta +'/'+ 'facturadetalle/htmf/lista_det.html';    
    ajax.metodo = "GET";            
    document.getElementById( "factura_detalles" ).innerHTML =  ajax.public.html();
    

    document.getElementById('factura_cliente').disabled = true;;  


                                        

    var orden_trabajo = document.getElementById('factura_orden_trabajo');            
    orden_trabajo.value = fmtNum(orden_trabajo.value);
    
    orden_trabajo.onblur  = function() {     

         
        orden_trabajo.value = fmtNum(orden_trabajo.value);      
        orden_trabajo.value = NumQP(orden_trabajo.value); 
        
        
        var  id = (orden_trabajo.value );


        ajax.url = html.url.absolute()+'/api/ordenestrabajos/sf/'+id;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();      


        var ojson = JSON.parse(datajson) ;    
        

        if (ajax.state == 200)
        {   
            obj.json = datajson;

            document.getElementById('factura_cliente').innerHTML = ojson.cliente.cliente; 
            
            document.getElementById('cliente_descripcion').innerHTML = 
                    ojson.cliente.nombre+ " " + ojson.cliente.apellido; 
            
            document.getElementById('ruc_descripcion').innerHTML = ojson.cliente.ruc; 
            
            document.getElementById('tel_descripcion').innerHTML = ojson.cliente.telefono;
            document.getElementById('direccion_descripcion').innerHTML = ojson.cliente.direccion;
            document.getElementById('ciudad_descripcion').innerHTML = ojson.cliente.ciudad;
            
            
            document.getElementById('total_iva0').innerHTML = ojson.suma.total_iva0;
            document.getElementById('total_iva0').innerHTML = fmtNum(document.getElementById('total_iva0').innerHTML);
            
            document.getElementById('total_iva5').innerHTML = ojson.suma.total_iva5;
            document.getElementById('total_iva5').innerHTML = fmtNum(document.getElementById('total_iva5').innerHTML);
            
            document.getElementById('total_iva10').innerHTML = ojson.suma.total_iva10;
            document.getElementById('total_iva10').innerHTML = fmtNum(document.getElementById('total_iva10').innerHTML);
            
            document.getElementById('total_factura').innerHTML = ojson.suma.total_factura;
            document.getElementById('total_factura').innerHTML = fmtNum(document.getElementById('total_factura').innerHTML);
                        
            
            factura_acciones_guardar( obj );
            
            
            
        }        
        else{            
            obj.json = "";

            document.getElementById('factura_cliente').innerHTML = ""; 
            
            document.getElementById('cliente_descripcion').innerHTML = "";
            
            document.getElementById('ruc_descripcion').innerHTML = "";
            
            document.getElementById('tel_descripcion').innerHTML = "";
            document.getElementById('direccion_descripcion').innerHTML = "";
            document.getElementById('ciudad_descripcion').innerHTML = "";
            
            
            document.getElementById('total_iva0').innerHTML = "0";
            document.getElementById('total_iva5').innerHTML = "0";
            document.getElementById('total_iva10').innerHTML = "0";
            document.getElementById('total_factura').innerHTML = "0";
            
        }   
    
        document.getElementById("chk_offset").disabled = true;
        document.getElementById("chk_tipografica").disabled = true;

        document.getElementById("chk_offset").checked = ojson.off_set;
        document.getElementById("chk_tipografica").checked = ojson.tipografica;

        //tabla.ini(obj);
        
        tabla.id = "detalle-tabla";
        tabla.linea = "id";
        tabla.tbody_id = "detalle-tb";
        tabla.campos = ['cantidad', 'cantidad_hoja', 'descripcion', 'precio_unitario',
            'porcentaje0', 'porcentaje5', 'porcentaje10' ];  
        //tabla.etiquetas = obj.etiquetas;        
        
        tabla.json =  JSON.stringify(ojson.detalles);
         
        tabla.gene();   
        
        var fac = new Object();
        fac.tablaformat = ['N', 'N', 'C', 'N',
            'N', 'N', 'N' ]; 
   
        
        tabla.formato(fac);
    
    
     };      
    






    var ico_more_orden = document.getElementById('ico-more-ordentrabajo');              
    ico_more_orden.onclick = function(  )
    {  
                
        var obj = new OrdenTrabajo();    
        obj.recurso = "ordenestrabajos;factura=0"
               
        obj.acctionresul = function(id) {               
            orden_trabajo.value = id;         
            orden_trabajo.onblur(); 
        };               
       
        modal.ancho = 800;
        busqueda.modal.objeto(obj);
        
    };       
    
    
    





    var cliente = document.getElementById('factura_cliente');            
    cliente.onblur  = function() {     

         
        cliente.value = fmtNum(cliente.value);      
        cliente.value = NumQP(cliente.value);          
        var  id = (cliente.value );

        ajax.url = html.url.absolute()+'/api/clientes/'+id;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();      
        

        if (ajax.state == 200)
        {            
            var oJson = JSON.parse(datajson) ;                  
            document.getElementById('cliente_descripcion').innerHTML = 
                    oJson['nombre'] + " " + oJson['apellido']; 
            
            document.getElementById('ruc_descripcion').innerHTML = oJson['ruc']; 
            document.getElementById('tel_descripcion').innerHTML = oJson['telefono']; 
            document.getElementById('direccion_descripcion').innerHTML = oJson['direccion']; 
            document.getElementById('ciudad_descripcion').innerHTML = oJson['ciudad']; 
            
            
        }
        
        else {
            document.getElementById('cliente_descripcion').innerHTML = "";
            document.getElementById('ruc_descripcion').innerHTML = "";
            document.getElementById('tel_descripcion').innerHTML = ""; 
            document.getElementById('direccion_descripcion').innerHTML = ""; 
            document.getElementById('ciudad_descripcion').innerHTML = "";             
        }   
    
    
     };      
    

    
    
           
    
};







Factura.prototype.post_form_id = function( obj  ) {    
    

    
    var ojson = JSON.parse(form.json) ; 
    
    var clienteId = ojson.cliente.cliente;
        
    
    var cliente = document.getElementById('factura_cliente');  
    cliente.value = clienteId;
    cliente.onblur();
    

    document.getElementById("chk_offset").disabled = true;
    document.getElementById("chk_tipografica").disabled = true;
    
    

    document.getElementById("chk_offset").checked = ojson.off_set;
    document.getElementById("chk_tipografica").checked = ojson.tipografica;    

 
 
    
    // cargar los detalles    
    var factura_detalle = new FacturaDetalle();
    factura_detalle.sublista(form.json);
    
    
    // total 
    document.getElementById('total_iva0').innerHTML = fmtNum( ojson.suma.total_iva0 ) ;  
    document.getElementById('total_iva5').innerHTML = fmtNum( ojson.suma.total_iva5 ) ;  
    document.getElementById('total_iva10').innerHTML = fmtNum( ojson.suma.total_iva10 ) ;  
    
    document.getElementById('total_factura').innerHTML = fmtNum( ojson.suma.total_factura ) ;  
     
    
}





Factura.prototype.main_list = function(obj, page) {    


    if (page === undefined) {    
        page = 1;
    }

    let promesa = arasa.vista.lista_paginacion(obj, page);
    
    promesa        
        .then(( xhr ) => {              
            arasa.html.url.redirect(xhr.status);                                                          
            // botones de accion - nuevo para este caso
            
            // botones de accion - nuevo para este caso       
            boton.objeto = ""+obj.tipo;
            document.getElementById( obj.tipo +'_acciones_lista' ).innerHTML 
                =  boton.basicform.get_botton_new();                
            
            var btn_objeto_nuevo = document.getElementById('btn_' + obj.tipo + '_nuevo');
            btn_objeto_nuevo.addEventListener('click',
                function(event) {  
                    obj.new( obj );
                },
                false
            );              
        
        })
       /*
        .catch(( xhr ) => { 
            console.log(xhr.message);
        }); 
*/


};








Factura.prototype.registro_botones = function(obj, id) {    


    form.name = "form_" + obj.tipo;            
    form.ocultar_foreign();

    boton.ini(obj);

    
    boton.blabels = ['Imprimir','Atras']    
    var strhtml =  boton.get_botton_base();     
    

    document.getElementById(  obj.tipo + '_acciones' ).innerHTML = strhtml;



    var btn_factura_imprimir = document.getElementById('btn_factura_imprimir');                    
    btn_factura_imprimir.addEventListener('click',
        function(event) {    
            
            
            //alert("falta agregar codigo")
            
            
            window.open(
             html.url.absolute() +"/FacturaVenta/Reporte?codigo="+id+"&mtotal=" 
               +qcoma(document.getElementById('total_factura').innerHTML),
             "_blank" );            

            
        },
        false
    );    



    var btn_factura_atras = document.getElementById('btn_factura_atras');                    
    btn_factura_atras.addEventListener('click',
        function(event) {       
            obj.main_list( obj, 1 );
        },
        false
    );    








};


