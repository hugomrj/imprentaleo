

/* global loader, ajax */

var arasa = 
{      

    html:{
            
        url:{
            
            redirect: function(val) 
            {
                
//alert(modsis)   
                var modsis =  localStorage.getItem('modsis');

    /*                                   
                var URLactual = window.location.href;
                
                var expression = "smbv/";
                var index = URLactual.search(expression);
                if(index == 0) {
                    expression = "";
                } 
*/


                switch(val) {
                    case 200:
                        break;                    
                    case 401:
                        window.location = arasa.html.url.absolute()+"/"+modsis;                                     
                    case 500:
                        window.location = arasa.html.url.absolute()+"/"+modsis ;                 
                        break;            
                    default:
                        window.location = arasa.html.url.absolute()+"/"+modsis ;                 
                }

            },              
            
            
            
            control_promise: function() 
            {

                const promise = new Promise((resolve, reject) => {
            
                                        
                    loader.inicio();

                    var comp = window.location.pathname;                            
                    var path =  comp.replace(arasa.html.url.absolute() , "");       

                    var xhr =  new XMLHttpRequest();            
                    var url = arasa.html.url.absolute() +"/api/usuarios/controlurl"; 
                    var metodo = "GET";                         

                    
                    xhr.open( metodo.toUpperCase(),   url,  true );      
                    
                    
                    xhr.onreadystatechange = function () {
                        if (this.readyState == 4 ){

                            //ajax.headers.get();
                            ajax.local.token =  xhr.getResponseHeader("token") ;            
                            localStorage.setItem('token', xhr.getResponseHeader("token") );     

                            ajax.state = xhr.status;
                            
                                resolve( xhr );

                            arasa.html.url.redirect(ajax.state);                            
                            
                            //arasa.html.barra_superior(xhr);  
                
                            loader.fin();


                        }
                    };
                    xhr.onerror = function (e) {                    
                        reject( xhr );                 
                        
                        alert("err");
                    };                       

                    xhr.setRequestHeader("path", path );
                    var type = "application/json";
                    xhr.setRequestHeader('Content-Type', type);   
                    //ajax.headers.set();
                    xhr.setRequestHeader("token", localStorage.getItem('token'));           

                    xhr.send( null );                       
                   
                    
 
                })

                return promise;


               

              },

            
        
            absolute: function() 
            {
                var pathname = window.location.pathname;
                pathname = pathname.substring(1,pathname.length);
                pathname = pathname.substring(0,pathname.indexOf("/"));
                return "/"+pathname ;            
            },               

        
        
        },
        



        barra_superior: function( xhr ) {  

            ajax.url = arasa.html.url.absolute()  + '/basicas/barrasuperior/barrasuperior.html';   
            ajax.metodo = "GET";            
            document.getElementById( "barrasuperior" ).innerHTML =  ajax.public.html();        

            document.getElementById( "session_usuario_cab" ).innerHTML = 
                    xhr.getResponseHeader("sessionusuario");

            document.getElementById( "session_sucursal_cab" ).innerHTML = 
                    xhr.getResponseHeader("sessionsucursal");    

        },





        paginacion: {  
            
            pagina:     1,     
    
            total_registros: 0,

            lineas:       12,
            cantidad_paginas:  0,

            pagina_inicio : 1,
            pagina_fin : 0, 
            bloques:    12,

            html: "",    

            ini: function(json) { 
                
                var ojson = JSON.parse(json) ;                                     

                
                            
                arasa.html.paginacion.total_registros = ojson["total_registros"];
                                
                var pagina = ojson["pagina"];
                                

                var cantidad_paginas;
                var total_registros = arasa.html.paginacion.total_registros ;
                var lineas = arasa.html.paginacion.lineas;
                var bloques = arasa.html.paginacion.bloques;
                var pagina_inicio = arasa.html.paginacion.pagina_inicio;
                var pagina_fin = arasa.html.paginacion.pagina_fin;
                


                cantidad_paginas = total_registros / lineas;
                cantidad_paginas = Math.trunc(cantidad_paginas);

                var calresto  = cantidad_paginas * lineas;
                if (calresto != total_registros) {
                    cantidad_paginas = cantidad_paginas + 1;
                }


                if ((cantidad_paginas * lineas ) < total_registros){
                    cantidad_paginas++;
                }        

                if (bloques > cantidad_paginas) {
                    pagina_inicio = 1;
                    pagina_fin = cantidad_paginas;
                }        
                else{
                    if (bloques == cantidad_paginas) {
                        pagina_inicio = 1;
                        pagina_fin = bloques;
                    }    
                    else{ 
                        if (bloques  < cantidad_paginas) {

                            if (pagina - (bloques / 2) < 1) {
                                pagina_inicio = 1;
                                pagina_fin = pagina + bloques;
                            }
                            else{
                                pagina_inicio = pagina - (bloques / 2);
                            }

                            if (pagina + (bloques / 2) > cantidad_paginas )                        {
                                pagina_inicio = cantidad_paginas - bloques;
                                pagina_fin = cantidad_paginas;
                            }
                            else{
                                pagina_fin =  pagina + (bloques / 2);
                            }    
                        }
                    }    
                }                 

                arasa.html.paginacion.pagina = pagina;
                arasa.html.paginacion.cantidad_paginas = cantidad_paginas;
                arasa.html.paginacion.total_registros = total_registros;
                arasa.html.paginacion.pagina_inicio = pagina_inicio;
                arasa.html.paginacion.pagina_fin = pagina_fin;


            },


            gene: function() {       

                //paginacion.total_registros  = 16
        
                var total_registros = arasa.html.paginacion.total_registros ;
                var cantidad_paginas = arasa.html.paginacion.cantidad_paginas ;
                var pagina = arasa.html.paginacion.pagina ;
                var pagina_inicio = arasa.html.paginacion.pagina_inicio ;
                var pagina_fin = arasa.html.paginacion.pagina_fin;


                var html;



                if (total_registros == 0){
                    return "";
                }

                if (cantidad_paginas == 1){
                    pagina = 1;
                }

                html = "<ul data-paginaactual=\"1\" id=\"paginacion\">";


                if (pagina != pagina_inicio){            
                    html += "<li data-pagina=\"ant\" >" ;

                    html +=      "<a href=\"javascript:void(0);\"id =\"ant\">" ;
                    html +=          "anterior" ;
                    html +=      "</a>" ;

                    html += "</li>";
                }



                for (i = pagina_inicio; i <= pagina_fin; i++) {   

                    if (pagina === i)
                    {                
                        html += "<li class=\"actual\" data-pagina=\"act\">" ;
                        html += "<a href=\"javascript:void(0);\" > " ;
                        html += i ;
                        html += "</a>" ;
                        html += "</li>" ;
                    }
                    else
                    {                
                        html += "<li data-pagina= \"pag" + i + "\"  >" ;
                        html += "<a href=\"javascript:void(0);\" id =pag"+ i +"> " ;
                        html += i ;
                        html += "</a>" ;
                        html += "</li>" ;                

                    }
                }

                if (pagina != pagina_fin){            
                    html += "<li data-pagina=\"sig\" >" ;

                    html +=      "<a href=\"javascript:void(0);\"id =\"sig\">" ;
                    html +=          "siguiente" ;
                    html +=      "</a>" ;

                    html += "</li>";
                }
                html += " </ul>"
                return html;

            },




            move: function(  obj, busca, fn ) {       



                var listaUL = document.getElementById( obj.tipo + "_paginacion" );
                var uelLI = listaUL.getElementsByTagName('li');
                
                
                var pagina = 0;
                
                
                for (var i=0 ; i < uelLI.length; i++)
                {
                    var lipag = uelLI[i];   
                    
                    if (lipag.dataset.pagina == "act"){                                     
                        pagina = lipag.firstChild.innerHTML;
                    }                    
                }
                


                for (var i=0 ; i < uelLI.length; i++)
                {
                    var datapag = uelLI[i].dataset.pagina;     

                    if (!(datapag == "act"  || datapag == "det"  ))
                    {
                        uelLI[i].addEventListener ( 'click',
                            function() {                                      

                                switch (this.dataset.pagina)
                                {
                                   case "sig": 
                                           pagina = parseInt(pagina) +1;
                                           break;                                                                          

                                   case "ant":                                     
                                           pagina = parseInt(pagina) -1;
                                           break;

                                   default:  
                                           pagina = this.childNodes[0].innerHTML.toString().trim();
                                           break;
                                }
                                pagina = parseInt( pagina , 10);                                                                       
                                
                                //arasa.html.paginacion.pagina = pagina;

                                tabla.refresh_promise( obj, pagina, busca, fn  ) ;

                            },
                            false
                        );                
                    }            
                }           

            },

        },
 
    },
                    

    vista:{
    
        lista_paginacion: function( obj, page ) 
        {

            const promise = new Promise((resolve, reject) => {

                loader.inicio();
                obj.page = page;

                var comp = window.location.pathname;                 
                var path =  comp.replace( arasa.html.url.absolute() , "");

                var xhr =  new XMLHttpRequest();           
                

                var filtro = ""        
                if(typeof obj.getUrlFiltro === 'function') {
                  filtro = obj.getUrlFiltro(obj);
                }                    
               
                
                var url = arasa.html.url.absolute() +"/api/"+obj.recurso; 
                url = url + filtro;
                url = url + "?page=" + obj.page;
                

                var metodo = "GET";                         
                ajax.json  = null;
                xhr.open( metodo.toUpperCase(),   url,  true );      

                xhr.onreadystatechange = function () {
                    if (this.readyState == 4 ){

                        //let promesa = arasa.vista.tabla_html(obj);
                        //promesa       

                        arasa.vista.tabla_html(obj)
                            .then(( text ) => {

                                // botones de accion - nuevo para este caso

                                ajax.local.token =  xhr.getResponseHeader("token") ;            
                                localStorage.setItem('token', xhr.getResponseHeader("token") );     
                                //sessionStorage.setItem('total_registros',  ajax.xhr.getResponseHeader("total_registros"));


                                var ojson = JSON.parse( xhr.responseText ) ;     
                                tabla.json = JSON.stringify(ojson['datos']) ;


                                tabla.ini(obj);
                                tabla.gene();   
                                tabla.formato(obj);

                                tabla.set.tablaid(obj);     
                                tabla.lista_registro(obj, reflex.form_id_promise ); 

                                var json_paginacion = JSON.stringify(ojson['paginacion']);

                                arasa.vista.paginacion_html(obj, json_paginacion );

                                ajax.state = xhr.status;

                                    resolve( xhr );

                                loader.fin();

                            })
                        
                        //arasa.vista.tabla_html(obj);
                    }
                };
                xhr.onerror = function (e) {                    
                    reject(
                          xhr.status,
                          xhr.response   
                    );                 

                };                       

                xhr.setRequestHeader("path", path );
                var type = "application/json";
                xhr.setRequestHeader('Content-Type', type);   

                //ajax.headers.set();
                xhr.setRequestHeader("token", localStorage.getItem('token'));           

                xhr.send( ajax.json  );                       


            })

            return promise;

        },



        

        paginacion_html: function( obj , json ) {   

            arasa.html.paginacion.ini(json);

            document.getElementById( obj.tipo + "_paginacion" ).innerHTML 
                    = arasa.html.paginacion.gene();   

            arasa.html.paginacion.move(obj, "", reflex.form_id);

        },





        tabla_html: function( obj ) {   

            const promise = new Promise((resolve, reject) => {

                if (!(obj.alias === undefined)) {    
                    ajax.url = html.url.absolute() + obj.carpeta +'/'+ obj.alias + '/htmf/lista.html';    
                }              
                else{
                    ajax.url = html.url.absolute() + obj.carpeta +'/'+ obj.tipo + '/htmf/lista.html';    
                }

                fetch( ajax.url )
                  .then(response => {
                    return response.text();
                  })
                  .then(data => {
                    document.getElementById( obj.dom ).innerHTML =  data;
                    reflex.getTituloplu(obj);                                  
                        resolve( data );
                  })
                  
            })

            return promise;


        },


        
        
        
        
    },    
    


    modal:{
        

        paginacion_html: function( obj , json, busca ) {   

            arasa.html.paginacion.ini(json);

            document.getElementById( obj.tipo + "_paginacion" ).innerHTML 
                    = arasa.html.paginacion.gene();   

            arasa.modal.paginacion_move(obj, busca);

        },



        paginacion_move: function(  obj, busca ) {       



            var listaUL = document.getElementById( obj.tipo + "_paginacion" );
            var uelLI = listaUL.getElementsByTagName('li');


            var pagina = 0;


            for (var i=0 ; i < uelLI.length; i++)
            {
                var lipag = uelLI[i];   

                if (lipag.dataset.pagina == "act"){                                     
                    pagina = lipag.firstChild.innerHTML;
                }                    
            }



            for (var i=0 ; i < uelLI.length; i++)
            {
                var datapag = uelLI[i].dataset.pagina;     

                if (!(datapag == "act"  || datapag == "det"  ))
                {
                    uelLI[i].addEventListener ( 'click',
                        function() {                                      

                            switch (this.dataset.pagina)
                            {
                               case "sig": 
                                       pagina = parseInt(pagina) +1;
                                       break;                                                                          

                               case "ant":                                     
                                       pagina = parseInt(pagina) -1;
                                       break;

                               default:  
                                       pagina = this.childNodes[0].innerHTML.toString().trim();
                                       break;
                            }
                            pagina = parseInt( pagina , 10);                                                                       

                            //arasa.html.paginacion.pagina = pagina;
                            busqueda.modal.tablabusqueda(obj, pagina, busca);

                        },
                        false
                    );                
                }            
            }           

        },
        
        
        
    },    
    
    
    busqueda:{
        
        
    }


};





