
function OrdenTrabajo(){
    
   this.tipo = "ordentrabajo";   
   this.recurso = "ordenestrabajos";   
   this.value = 0;
   this.from_descrip = "";
   this.json_descrip = ['', ''];
   
   this.dom="";
   this.carpeta=  "/comercial";   
   
   this.titulosin = "Orden de Trabajo";
   this.tituloplu = "Ordenes de Trabajo";      
    
    
   this.campoid=  'orden_trabajo';
   
   this.tablacampos =  ['orden_trabajo', 'fecha_recepcion', 'fecha_entrega',
                                'cliente.cliente', 'cliente.nombre', 'cliente.apellido', 'factura'];   
   
    
   this.etiquetas =   ['Orden Trabajo', 'Fecha Recepcion', 'Fecha Entrega',
                                'Cliente', 'Nombre', 'Apellido', 'Factura'];   
   
   
   this.tablaformat =  [ 'N', 'D', 'D', 
                                    'N', 'C', 'C', 'C' ];   
      
   this.tbody_id = "ordentrabajo-tb";
      
   this.botones_lista = [ this.new] ;
   this.botones_form = "ordentrabajo-acciones";   
      
   this.tabs =  [];
   this.parent = null;
   
}




OrdenTrabajo.prototype.new = function( obj  ) {                
  
    ordentrabajo_formbasico( obj );
    obj.form_ini(obj);
  
    var cliente = document.getElementById('ordentrabajo_cliente');      
    cliente.onblur();          
    
    
    boton.objeto = "" + obj.tipo;        

    boton.ini(obj);
    document.getElementById( obj.tipo +'-acciones' ).innerHTML 
                =  boton.basicform.get_botton_add();        

            

    var btn_ordentrabajo_guardar = document.getElementById('btn_ordentrabajo_guardar');
    btn_ordentrabajo_guardar.addEventListener('click',
        function(event) {                
            
            if ( obj.form_validar()) {                
            
                form.name = "form_ordentrabajo";               
                var json_orden =  form.datos.getjson() ;             
                
                json_orden = json_orden.substring(0, json_orden.trim().length - 1 ) ;
                
                json_orden = json_orden  + ", detalles : " ;
                
                json_orden = json_orden + JSON.stringify(datadet) ;
                
                json_orden = json_orden  + ", valores : " ;
                
                json_orden = json_orden + JSON.stringify(data_valores) ;

                json_orden = json_orden  + " } " ;


//console.log(json_orden);


                ajax.metodo = "post";
                ajax.url = html.url.absolute()+"/api/"+ obj.recurso;               

                var data = ajax.private.json( json_orden );  

                switch (ajax.state) {

                  case 200:

                        msg.ok.mostrar("Registro agregado");          
                        reflex.data  = data;                        
                        
                        reflex.form_id( obj, reflex.getJSONdataID( obj.campoid ) ) ;     
                        
                        break; 

                  case 500:

                        msg.error.mostrar( data );          
                        break; 

                  default: 
                    msg.error.mostrar("error de acceso");           
                }                        

                
            }

            
        },
        false
    );    
 
     


    var btn_ordentrabajo_cancelar = document.getElementById('btn_ordentrabajo_cancelar');
    btn_ordentrabajo_cancelar.addEventListener('click',
        function(event) {    

            reflex.lista_paginacion(obj, 1);
            
        },
        false
    );    
 
     
    
    
  
};





OrdenTrabajo.prototype.form_validar = function() {   


    var fecha_recepcion = document.getElementById('ordentrabajo_fecha_recepcion');
    if (fecha_recepcion.value == ""){
        msg.error.mostrar("Falta fecha recepcion");                    
        fecha_recepcion.focus();
        fecha_recepcion.select();                                       
        return false;        
    }

    var fecha_entrega = document.getElementById('ordentrabajo_fecha_entrega');
    if (fecha_entrega.value == ""){
        msg.error.mostrar("Falta fecha entrega");                    
        fecha_entrega.focus();
        fecha_entrega.select();                                       
        return false;        
    }


    //var vdet = JSON.parse(datadet);
    if ( datadet.length == 0 ){
        msg.error.mostrar("Faltan cargar detalles");                    
        return false;        
    }


    if ( data_valores.length == 0 ){
        msg.error.mostrar("Faltan cargar valores");                    
        return false;        
    }    
   
    return true;
};











OrdenTrabajo.prototype.form_ini = function(obj) {    


    var obj = new OrdenTrabajo();

    // aca tengo que cargar los html
    ajax.url = html.url.absolute() + obj.carpeta +'/'+ 'ordentrabajodetalle/htmf/lista_det.html';    
    ajax.metodo = "GET";            
    document.getElementById( "orden_detalles" ).innerHTML =  ajax.public.html();
    
    
    
    // aca tengo que cargar los html
    ajax.url = html.url.absolute() + obj.carpeta +'/'+ 'ordentrabajovalores/htmf/lista_det.html';    
    ajax.metodo = "GET";            
    document.getElementById( "orden_valores" ).innerHTML =  ajax.public.html();
    




    var cliente = document.getElementById('ordentrabajo_cliente');            
    cliente.onblur  = function() {     

         
        cliente.value = fmtNum(cliente.value);      
        cliente.value = NumQP(cliente.value);          
        var  id = (cliente.value );

        ajax.url = html.url.absolute()+'/api/clientes/'+id;
        ajax.metodo = "GET";   
        var datajson = ajax.private.json();      
        

        if (ajax.state == 200)
        {            
            var oJson = JSON.parse(datajson) ;                  
            document.getElementById('cliente_descripcion').innerHTML = 
                    oJson['nombre'] + " " + oJson['apellido']; 
            
            document.getElementById('ruc_descripcion').innerHTML = oJson['ruc']; 
            document.getElementById('tel_descripcion').innerHTML = oJson['telefono']; 
            document.getElementById('direccion_descripcion').innerHTML = oJson['direccion']; 
            document.getElementById('ciudad_descripcion').innerHTML = oJson['ciudad']; 
            
            
        }
        
        else {
            document.getElementById('cliente_descripcion').innerHTML = "";
            document.getElementById('ruc_descripcion').innerHTML = "";
            document.getElementById('tel_descripcion').innerHTML = ""; 
            document.getElementById('direccion_descripcion').innerHTML = ""; 
            document.getElementById('ciudad_descripcion').innerHTML = "";             
        }   
    
    
     };      
    






    var ico_more_cliente = document.getElementById('ico-more-cliente');              
    ico_more_cliente.onclick = function(  )
    {  
        var obj = new Cliente();    
        obj.acctionresul = function(id) {    
            cliente.value = id; 
            cliente.onblur(); 
        };               
        
        modal.ancho = 700;
        busqueda.modal.objeto(obj);
    };       
    
    
    
   
    


    var detalle_agregar = document.getElementById('detalle_agregar');              
    detalle_agregar.onclick = function(  )
    {  
        
        ajax.url = html.url.absolute()+'/comercial/ordentrabajodetalle/htmf/form.html'; 
        ajax.metodo = "GET";  
        modal.ancho = 800;
        var obj = modal.ventana.mostrar("ind");       

        
        var obj_det = new OrdenTrabajoDetalle();            
        obj_det.new(obj_det);
        
        
    };       
    
    
       
    
    
    
    var agregar_valor = document.getElementById('agregar_valor');              
    agregar_valor.onclick = function(  )
    {  
        var obj_valores = new OrdenTrabajoValor();            
        obj_valores.new(obj_valores);       
        
    };       
    
    
           
    
};







OrdenTrabajo.prototype.post_form_id = function( obj  ) {    
    
    
    document.getElementById('orden_detalles_accion').style.display = "none";  
    document.getElementById('valores_detalles_accion').style.display = "none";  
    
    document.getElementById('orden_detalles').style.cssText = "margin-bottom: 40px";  
    document.getElementById('orden_valores').style.cssText = "margin-bottom: 40px";      
    
    
    
    var ojson = JSON.parse(form.json) ; 
    
    var cliente = ojson["cliente"]["cliente"] ;
    
    var ordentrabajo_cliente = document.getElementById('ordentrabajo_cliente');  
    ordentrabajo_cliente.value = cliente;
    ordentrabajo_cliente.onblur();
    
    
    document.getElementById( obj.tipo +'-acciones' ).innerHTML 
                =  boton.basicform.get_botton_reg2nl();  
        
    
    var btn_ordentrabajo_nuevo = document.getElementById('btn_ordentrabajo_nuevo');              
    btn_ordentrabajo_nuevo.onclick = function(  )
    {  
        obj.new(obj);
    };       
    
    
    var btn_ordentrabajo_lista = document.getElementById('btn_ordentrabajo_lista');              
    btn_ordentrabajo_lista.onclick = function(  )
    {  
        reflex.lista_paginacion(obj, 1);
    };       
    

    
    // cargar los detalles
    
    var order_detalle = new OrdenTrabajoDetalle();
    order_detalle.sublista(form.json);
    
    var order_valor = new OrdenTrabajoValor();
    order_valor.sublista(form.json);
    
    
        
        
        
    
}



