/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordentrabajovalor;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.util.Datetime;
import py.com.base.aplicacion.cliente.Cliente;


/**
 *
 * @author hugom_000
 */

public class OrdenTrabajoValorDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public OrdenTrabajoValorDAO ( ) throws IOException  {
    }
      
    
    
    
    public void add_lote (JsonObject jsonObject, Integer orden) 
            throws Exception{
    
        OrdenTrabajoValor obj = new OrdenTrabajoValor();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
        
               
        
        JsonArray jsonArray = jsonObject.get("valores").getAsJsonArray(); 
        
        for (JsonElement jsonelement : jsonArray) {
        
            obj.setOrden(orden);
            
            obj.setTam_ancho(jsonelement.getAsJsonObject().get("tam_ancho").getAsInt());
            obj.setTam_largo(jsonelement.getAsJsonObject().get("tam_largo").getAsInt());
            obj.setCorte_ancho(jsonelement.getAsJsonObject().get("corte_ancho").getAsInt());
            obj.setCorte_largo(jsonelement.getAsJsonObject().get("corte_largo").getAsInt());
            obj.setDescripcion(jsonelement.getAsJsonObject().get("descripcion").getAsString() );
            obj.setPliegos(jsonelement.getAsJsonObject().get("pliegos").getAsInt() );
            obj.setSale(jsonelement.getAsJsonObject().get("sale").getAsInt() );
            obj.setCantidad_pedido(jsonelement.getAsJsonObject().get("cantidad_pedido").getAsInt() );
            
            obj = (OrdenTrabajoValor) persistencia.insert(obj);      
            
        }     
    }
 
    
    


    public List<OrdenTrabajoValor>  lista (Integer orden) throws Exception {
                
        
        String sql = "";
        sql = sql + new OrdenTrabajoValorSQL().filtro_orden(orden);     
        
        
        List<OrdenTrabajoValor>  lista = null;        
        try {    
            
            ResultadoSet rs = new ResultadoSet();      
            
            lista = new Coleccion<OrdenTrabajoValor>().resultsetToList(
                    new OrdenTrabajoValor(),
                    rs.resultset(sql)
            );                        
                  
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
         
    
    
    
    
}
