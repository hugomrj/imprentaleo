/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordentrabajovalor;


import py.com.base.aplicacion.ordentrabajodetalle.*;
import nebuleuse.ORM.sql.ReaderT;

/**
 *
 * @author hugo
 */
public class OrdenTrabajoValorSQL {    
    
    
    
    public String filtro_orden (Integer orden )
            throws Exception {
    
        String sql = "";                                 
        
        
        ReaderT reader = new ReaderT("OrdenTrabajoValor");
        reader.fileExt = "filtro_orden.sql";
        
        sql = reader.get( orden );    
        
        return sql ;             
    
    }   
    
    
    
}
