/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.facturadetalle;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.ResultadoSet;



/**
 *
 * @author hugom_000
 */

public class FacturaDetalleDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public FacturaDetalleDAO ( ) throws IOException  {
    }
      
    
    
    
    
    
    public void add_lote (JsonObject jsonObject, Integer factura) 
            throws Exception{
    
        FacturaDetalle obj = new FacturaDetalle();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
        
               
        
        JsonArray jsonArray = jsonObject.get("detalles").getAsJsonArray(); 
        
        for (JsonElement jsonelement : jsonArray) {
        
            obj.setFactura(factura);
            
            obj.setCantidad_hoja(jsonelement.getAsJsonObject().get("cantidad_hoja").getAsInt());
            obj.setCantidad(jsonelement.getAsJsonObject().get("cantidad").getAsInt());
            obj.setDescripcion( jsonelement.getAsJsonObject().get("descripcion").getAsString()  );
            obj.setPrecio_unitario(jsonelement.getAsJsonObject().get("precio_unitario").getAsLong()  );
            obj.setImpuesto(jsonelement.getAsJsonObject().get("impuesto").getAsInt() );
            obj.setSub_total(jsonelement.getAsJsonObject().get("sub_total").getAsLong() );
            obj.setPorcentaje0(jsonelement.getAsJsonObject().get("porcentaje0").getAsInt() );
            obj.setPorcentaje5(jsonelement.getAsJsonObject().get("porcentaje5").getAsInt() );
            obj.setPorcentaje10(jsonelement.getAsJsonObject().get("porcentaje10").getAsInt() );
        
            
            obj = (FacturaDetalle) persistencia.insert(obj);      
            
        }     
    }
 
    
    
    
    
    
    

    public List<FacturaDetalle>  lista (Integer orden) throws Exception {
                
        
        String sql = "";
        sql = sql + new FacturaDetalleSQL().filtro_orden(orden);     
        
        
        List<FacturaDetalle>  lista = null;        
        try {    
            
            ResultadoSet rs = new ResultadoSet();      
            
            lista = new Coleccion<FacturaDetalle>().resultsetToList(
                    new FacturaDetalle(),
                    rs.resultset(sql)
            );                        
                  
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
     
    

    
}
