/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordentrabajodetalle;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;


public class OrdenTrabajoDetalleJSON  {


    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
    
    
    
    public OrdenTrabajoDetalleJSON ( ) throws IOException  {
    
    }
      
    
    
    

    public JsonArray  lista_orden ( Integer orden ) {
                
        JsonArray jsonArray = new JsonArray();

        try 
        {   
  
            
            OrdenTrabajoDetalleDAO dao = new OrdenTrabajoDetalleDAO();            
            List<OrdenTrabajoDetalle> lista = dao.lista(orden);

            
            //datos            
            JsonParser jsonParser = new JsonParser();
            jsonArray = (JsonArray) jsonParser.parse(gson.toJson( lista ));    
            
     
        }         
        catch (Exception ex) {                   
            
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return jsonArray ;         
        }
    }      
    
    
    
    
 
        
    
    
    
        
}
