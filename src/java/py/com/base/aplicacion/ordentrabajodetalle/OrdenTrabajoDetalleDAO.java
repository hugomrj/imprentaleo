/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordentrabajodetalle;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.util.Datetime;
import py.com.base.aplicacion.cliente.Cliente;


/**
 *
 * @author hugom_000
 */

public class OrdenTrabajoDetalleDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public OrdenTrabajoDetalleDAO ( ) throws IOException  {
    }
      
    
    
    
    public void add_lote (JsonObject jsonObject, Integer orden) 
            throws Exception{
    
        OrdenTrabajoDetalle obj = new OrdenTrabajoDetalle();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
        
               
        
        JsonArray jsonArray = jsonObject.get("detalles").getAsJsonArray(); 
        
        for (JsonElement jsonelement : jsonArray) {
            
        //    System.out.println( jsonelement.getAsJsonObject().get("cantidad_hoja") );   
        
        
            obj.setOrden(orden);
            
            obj.setCantidad_hoja(jsonelement.getAsJsonObject().get("cantidad_hoja").getAsInt());
            obj.setCantidad(jsonelement.getAsJsonObject().get("cantidad").getAsInt());
            obj.setDescripcion( jsonelement.getAsJsonObject().get("descripcion").getAsString()  );
            obj.setPrecio_unitario(jsonelement.getAsJsonObject().get("precio_unitario").getAsLong()  );
            obj.setImpuesto(jsonelement.getAsJsonObject().get("impuesto").getAsInt() );
            obj.setSub_total(jsonelement.getAsJsonObject().get("sub_total").getAsLong() );
            obj.setPorcentaje0(jsonelement.getAsJsonObject().get("porcentaje0").getAsInt() );
            obj.setPorcentaje5(jsonelement.getAsJsonObject().get("porcentaje5").getAsInt() );
            obj.setPorcentaje10(jsonelement.getAsJsonObject().get("porcentaje10").getAsInt() );
        
            
            obj = (OrdenTrabajoDetalle) persistencia.insert(obj);      
            
            //ordentrabajo.setOff_set( jsonObject.get("off_set").getAsBoolean() );
        
            ///dao = (OrdenTrabajoDetalle) persistencia.insert(dao); 
        }     
    }
 
    
    

    public List<OrdenTrabajoDetalle>  lista (Integer orden) throws Exception {
                
        
        String sql = "";
        sql = sql + new OrdenTrabajoDetalleSQL().filtro_orden(orden);     
        
        
        List<OrdenTrabajoDetalle>  lista = null;        
        try {    
            
            ResultadoSet rs = new ResultadoSet();      
            
            lista = new Coleccion<OrdenTrabajoDetalle>().resultsetToList(
                    new OrdenTrabajoDetalle(),
                    rs.resultset(sql)
            );                        
                  
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
     
    

    
}
