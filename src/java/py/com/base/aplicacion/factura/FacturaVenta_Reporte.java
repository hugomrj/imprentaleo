/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.aplicacion.factura;



import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import nebuleuse.ORM.postgres.Conexion;

import nebuleuse.util.Convercion;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

/**
 *
 * @author hugo
 */

@WebServlet(name = "FacturaVenta_Reporte", 
        urlPatterns = {"/FacturaVenta/Reporte"})


public class FacturaVenta_Reporte extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        
        try { 
            
            
            
            String archivo = "FacturaVenta";
            //String archivo_jasper = archivo+".jasper";
            String archivo_jrxml = archivo+".jrxml";
            
        
            response.setHeader("Content-disposition","inline; filename="+archivo+".pdf");
            response.setContentType("application/pdf");
            
            
            
            Integer codigo = 0;
            codigo = Integer.parseInt(request.getParameter("codigo")) ;  
            
            
            //monto total
            Long monto_total = 0L;
            // falta quitar los puntos
            monto_total  = Long.parseLong(request.getParameter("mtotal")) ;  
            
            
 
            Conexion cnx = new Conexion();
            cnx.conectar();
            String url =  request.getServletContext().getRealPath("/WEB-INF")+"/jasper/";
            url = url + archivo_jrxml;
                    
            // parametros
            Map<String, Object> parameters = new HashMap<String, Object>();
                        
            parameters.put("par_factura", codigo);
            
            Convercion conversion = new Convercion();            
            parameters.put("par_monto_letras", conversion.numeroaLetras(monto_total));
            
            
            
            JasperReport report = JasperCompileManager.compileReport(url);            
            JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, cnx.getConexion());
        
          
            ServletOutputStream servletOutputStream = response.getOutputStream();
            byte[] reportePdf = JasperExportManager.exportReportToPdf(jasperPrint);

            response.setContentLength(reportePdf.length);
            
            servletOutputStream.write(reportePdf, 0, reportePdf.length);
            servletOutputStream.flush();
            servletOutputStream.close();    
            
            
            
        } catch (JRException ex) {
            Logger.getLogger(FacturaVenta_Reporte.class.getName()).log(Level.SEVERE, null, ex);
        }

            
            
            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(FacturaVenta_Reporte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(FacturaVenta_Reporte.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
