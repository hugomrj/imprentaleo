/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.factura;

import java.util.Date;
import py.com.base.aplicacion.cliente.Cliente;

/**
 * @author hugo
 */


public class Factura {
    
    private Integer factura;
    private Integer numero_factura;
    private Date fecha_factura;
    private Cliente cliente;
    private Integer gravada0;
    private Integer gravada5;
    private Integer gravada10;
    private Integer iva5;
    private Integer iva10;
    private Integer monto_total;
    private Integer usuario;
    private String  forma_pago;
    private Integer orden_trabajo;
    private Integer total_iva;
    
    private Boolean off_set;
    private Boolean tipografica;    

    public Integer getFactura() {
        return factura;
    }

    public void setFactura(Integer factura) {
        this.factura = factura;
    }

    public Integer getNumero_factura() {
        return numero_factura;
    }

    public void setNumero_factura(Integer numero_factura) {
        this.numero_factura = numero_factura;
    }

    public Date getFecha_factura() {
        return fecha_factura;
    }

    public void setFecha_factura(Date fecha_factura) {
        this.fecha_factura = fecha_factura;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Integer getGravada0() {
        return gravada0;
    }

    public void setGravada0(Integer gravada0) {
        this.gravada0 = gravada0;
    }

    public Integer getGravada5() {
        return gravada5;
    }

    public void setGravada5(Integer gravada5) {
        this.gravada5 = gravada5;
    }

    public Integer getGravada10() {
        return gravada10;
    }

    public void setGravada10(Integer gravada10) {
        this.gravada10 = gravada10;
    }

    public Integer getIva5() {
        return iva5;
    }

    public void setIva5(Integer iva5) {
        this.iva5 = iva5;
    }

    public Integer getIva10() {
        return iva10;
    }

    public void setIva10(Integer iva10) {
        this.iva10 = iva10;
    }

    public Integer getMonto_total() {
        return monto_total;
    }

    public void setMonto_total(Integer monto_total) {
        this.monto_total = monto_total;
    }

    public Integer getUsuario() {
        return usuario;
    }

    public void setUsuario(Integer usuario) {
        this.usuario = usuario;
    }

    public String getForma_pago() {
        return forma_pago;
    }

    public void setForma_pago(String forma_pago) {
        this.forma_pago = forma_pago;
    }

    public Integer getOrden_trabajo() {
        return orden_trabajo;
    }

    public void setOrden_trabajo(Integer orden_trabajo) {
        this.orden_trabajo = orden_trabajo;
    }

    public Integer getTotal_iva() {
        return total_iva;
    }

    public void setTotal_iva(Integer total_iva) {
        this.total_iva = total_iva;
    }

    public Boolean getOff_set() {
        return off_set;
    }

    public void setOff_set(Boolean off_set) {
        this.off_set = off_set;
    }

    public Boolean getTipografica() {
        return tipografica;
    }

    public void setTipografica(Boolean tipografica) {
        this.tipografica = tipografica;
    }
    

    
}



