/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.factura;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.db.JsonObjeto;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.sql.SentenciaSQL;


public class FacturaJSON  {

    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   

    
    
    public FacturaJSON ( ) throws IOException  {
    
    }
      
    
    
    

    public JsonObject  lista ( Integer page, String buscar) {        
        
        JsonObject jsonObject = new JsonObject();
        
        try 
        {     

            ResultadoSet resSet = new ResultadoSet();        
            
            String sql = "";
            String sqlFiltro = "";
            String sqlOrder = "";
                        
            if (buscar == null) {                
                sql = SentenciaSQL.select(new Factura());    
            }
            else{                
                sql =  SentenciaSQL.select(new Factura());   
                sqlFiltro =  new FacturaSQL().filtro(buscar);   ;       
            }        
   
            sqlOrder =  " order by factura desc  ";
            
            sql = sql + sqlFiltro + sqlOrder ;

           
            // ResultSet rsData = resSet.resultset(sql, page);                
            FacturaDAO dao = new FacturaDAO();
            List<Factura> lista = dao.list(sql, page);
            
          

            JsonArray jsonarrayDatos = new JsonArray();
            JsonParser jsonParser = new JsonParser();
            jsonarrayDatos = (JsonArray) jsonParser.parse(gson.toJson( lista ));        
            
            
            
            // paginacipon
            JsonObject jsonPaginacion = new JsonObject();            
            jsonPaginacion = new JsonObjeto().json_paginacion(sql, page);
            
            
            // union de partes
            jsonObject.add("paginacion", jsonPaginacion);
            
            jsonObject.add("datos", jsonarrayDatos);    
            //jsonObject.add("summary", jsonarraySuma);            
            
     
        }         
        catch (Exception ex) {                   
            throw new Exception(ex);
        }
        finally
        {
            return jsonObject ;         
        }
    }      
    
    
    
        

    
    
        
}
