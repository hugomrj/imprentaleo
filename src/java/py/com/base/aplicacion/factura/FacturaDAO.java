/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.factura;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.beans.Statement;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.db.ResultadoSet;
import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.util.Datetime;
import py.com.base.aplicacion.facturadetalle.FacturaDetalleDAO;


/**
 *
 * @author hugom_000
 */

public class FacturaDAO  {
    
    private Persistencia persistencia = new Persistencia();      
    
    
    public FacturaDAO ( ) throws IOException  {
    }
      
    
    
    
    public List<Factura>  list (String sql, Integer page) {
                
        List<Factura>  lista = null;        
        try {                        
                        
            ResultadoSet resSet = new ResultadoSet();   
            
            
            ResultSet rsData = resSet.resultset(sql, page);   
            
            
            lista = new Coleccion<Factura>().resultsetToList(
                    new Factura(),
                    rsData                    
            );                      
            
            
        }         
        catch (Exception ex) {                                    
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

     
    
    

    
    
    public Factura add (String json) throws Exception{
    
        //Factura factura = new Factura();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
                
        JsonObject jsonObject = gson.fromJson(json, JsonObject.class);    

        Factura req = gson.fromJson(json, Factura.class);    

        req.setNumero_factura(0);    
        
        req = (Factura) persistencia.insert(req);             
        
        // detalles
        new FacturaDetalleDAO().add_lote(jsonObject, req.getFactura() );        

        
        updateCabecera(req.getFactura());
        
        
        
/*        

        ordentrabajo.setFecha_entrega(
                Datetime.castDate( jsonObject.get("fecha_entrega").getAsString()));    
        Cliente cliente = new Cliente();
        cliente.setCliente(jsonObject.get("cliente").getAsInt());
        ordentrabajo.setCliente(cliente);        
        
        ordentrabajo.setFactura(0);        
        ordentrabajo = (OrdenTrabajo) persistencia.insert(ordentrabajo);      
                
        // detalles
        new OrdenTrabajoDetalleDAO().add_lote(jsonObject, ordentrabajo.getOrden_trabajo() );
        
        // valores
        new OrdenTrabajoValorDAO().add_lote(jsonObject, ordentrabajo.getOrden_trabajo() );
        
*/        
        
        
        return req ;
    }
 
    
    
    
    
    public void updateCabecera ( Integer factura  ) throws SQLException   {
    
        Conexion conexion = new Conexion();
        conexion.conectar();            
        

        String sql = 
            "UPDATE facturas\n" +
            "SET monto_total = (\n" +
            "    SELECT sum(sub_total)\n" +
            "    FROM facturas_detalle\n" +
            "    WHERE factura = " + factura + "\n" +
            "),\n" +
            "gravada0 = (\n" +
            "    SELECT sum(porcentaje0)\n" +
            "    FROM facturas_detalle\n" +
            "    WHERE factura = " + factura + "\n" +
            "),\n" +
            "gravada5 = (\n" +
            "    SELECT sum(porcentaje5)\n" +
            "    FROM facturas_detalle\n" +
            "    WHERE factura = " + factura + "\n" +
            "),\n" +
            "gravada10 = (\n" +
            "    SELECT sum(porcentaje10)\n" +
            "    FROM facturas_detalle\n" +
            "    WHERE factura = " + factura + "\n" +
            ")\n" +
            "WHERE factura = " + factura;
        
        conexion.getConexion().createStatement().executeLargeUpdate(sql);                           
        


        String sql2 = 
            "UPDATE facturas\n" +
            "SET \n" +
            "    iva5 = ROUND(gravada5 / 21::numeric, 2),\n" +
            "    iva10 = ROUND(gravada10 / 11::numeric, 2),\n" +
            "    total_iva = ROUND((gravada5 / 21::numeric) + (gravada10 / 11::numeric), 2)\n" +
            "WHERE factura = " + factura;

        conexion.getConexion().createStatement().executeLargeUpdate(sql2);     

        
    }      
    
    
        
  
    

    
}
