/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.base.aplicacion.factura;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.MatrixParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.seguridad.Autentificacion;
import py.com.base.aplicacion.ordentrabajo.OrdenTrabajoDAO;


/**
 * REST Web Service
 * @author hugo
 */


@Path("facturas")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class FacturaWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();    
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
    private Response.Status status  = Response.Status.OK;
    
    Factura com = new Factura();       
                         
    
    
    public FacturaWS() {        
        
    }

       
    
    
    
    @GET    
    public Response lista ( 
            @HeaderParam("token") String strToken,
            @MatrixParam("q") String q,
            @QueryParam("page") Integer page) {
        
            if (page == null) {                
                page = 1;
            }

        try {                    
           
            if (autorizacion.verificar(strToken))
            {                
                autorizacion.actualizar();                                
                
                JsonObject jsonObject ;                                                
                if (q == null) {                                
                    jsonObject = new FacturaJSON().lista(page, null);
                } 
                else{
                    jsonObject = new FacturaJSON().lista(page, q);
                }                
                  
                
                return Response
                        .status(Response.Status.OK)
                        .entity(jsonObject.toString())
                        .header("token", autorizacion.encriptar())
                        .build();                       
            }
            else{
                
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();   
            }        
        }     
        catch (Exception ex) {
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error")
                    .header("token", null)
                    .build();                                        
        }      
    }    
    
      
    
  
    
    
@POST
public Response add( 
        @HeaderParam("token") String strToken,
        String json ) {   

    try {

        
        if (autorizacion.verificar(strToken)) {
            autorizacion.actualizar();                    

            
            
            JsonObject jsonObject = new Gson().fromJson(json, JsonObject.class);
            int orden_trabajo = jsonObject.get("orden_trabajo").getAsInt();

            
            // Lanza una excepción si factura es distinto de 0
            /*
            if (factura > 0) {
                throw new IllegalArgumentException("La orden de trabajo ya está asociada a una factura");
            }          
            */
            
            
            
            
            FacturaDAO dao = new FacturaDAO();
            this.com = dao.add(json);                
            
            
            
            
            OrdenTrabajoDAO ordenDAO = new OrdenTrabajoDAO();            
            ordenDAO.updateFactura( this.com.getFactura() , 
                    this.com.getOrden_trabajo());
            
            
            
            
            if (this.com == null) {
                return Response
                        .status(Response.Status.NO_CONTENT)
                        .header("token", autorizacion.encriptar())
                        .build();
            }

            json = gson.toJson(this.com);

            return Response
                    .status(Response.Status.OK) // Código 200 OK
                    .entity(json)
                    .header("token", autorizacion.encriptar())
                    .build();       
        } 
        else {                 
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();             
        }

    } 
    
    catch (Exception ex) {
        // Determina el código de estado basado en el tipo de excepción
        Response.Status status = (ex instanceof IllegalArgumentException)
            ? Response.Status.BAD_REQUEST // 400 Bad Request
            : Response.Status.INTERNAL_SERVER_ERROR; // 500 Internal Server Error

        return Response
                .status(status)
                .entity(ex.getMessage())
                .header("token", autorizacion.encriptar())
                .build();                                        
    }        
}





        

                 
    
    
    @GET
    @Path("/{id}")
    public Response get(     
            @HeaderParam("token") String strToken,
            @PathParam ("id") Integer id ) {
                     
        try 
        {                  
            if (autorizacion.verificar(strToken))
            {
                autorizacion.actualizar();                
                
                FacturaExt ext = new FacturaExt();

                ext = (FacturaExt) persistencia.filtrarId(ext, id );  

                String json = gson.toJson(ext);


                if (ext == null){
                    this.status = Response.Status.NO_CONTENT;                           
                }

                
                return Response
                        .status( this.status )
                        .entity(json)
                        .header("token", autorizacion.encriptar())
                        .build();       
            }
            else{
                return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .header("token", null)
                    .build();   
            }
        
        }     
        catch (Exception ex) {            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(ex.getMessage())
                    .header("token", autorizacion.encriptar())
                    .build();           
        }          
    }    
      
        
    
    
    
    

    
}