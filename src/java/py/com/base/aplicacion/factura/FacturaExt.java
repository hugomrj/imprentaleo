/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.factura;


import py.com.base.aplicacion.ordentrabajo.*;
import com.google.gson.JsonArray;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import py.com.base.aplicacion.facturadetalle.FacturaDetalle;
import py.com.base.aplicacion.facturadetalle.FacturaDetalleDAO;




/**
 *
 * @author hugo
 */

public class FacturaExt extends Factura{
    
    
    private List<FacturaDetalle> detalles = new ArrayList <FacturaDetalle>() ;   
    
    private Map<String, Long> suma = new HashMap<>();

    
   
    public void extender() throws IOException, Exception {



        FacturaDetalleDAO detallesDAO = new FacturaDetalleDAO();
        this.detalles = detallesDAO.lista(this.getFactura());
                        
        
        long suma_total_iva0 = 0L;
        long suma_total_iva5 = 0L;
        long suma_total_iva10 = 0L;
        long suma_total = 0L;
        
        
        for (Iterator<FacturaDetalle> iterator = this.detalles.iterator(); iterator.hasNext();) {
            FacturaDetalle det = iterator.next();
            
            suma_total_iva0 = suma_total_iva0 + det.getPorcentaje0();
            suma_total_iva5 = suma_total_iva5 + det.getPorcentaje5();
            suma_total_iva10 = suma_total_iva10 + det.getPorcentaje10();
            suma_total = suma_total + det.getSub_total();
            
        }
        
        
        this.suma.put("total_iva0", suma_total_iva0);
        this.suma.put("total_iva5", suma_total_iva5);
        this.suma.put("total_iva10", suma_total_iva10);        
        this.suma.put("total_factura", suma_total);
        
        
        
    }


   
     
            
}
