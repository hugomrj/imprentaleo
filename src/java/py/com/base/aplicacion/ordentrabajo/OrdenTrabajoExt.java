/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordentrabajo;


import com.google.gson.JsonArray;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import py.com.base.aplicacion.ordentrabajodetalle.OrdenTrabajoDetalle;
import py.com.base.aplicacion.ordentrabajodetalle.OrdenTrabajoDetalleDAO;
import py.com.base.aplicacion.ordentrabajodetalle.OrdenTrabajoDetalleJSON;
import py.com.base.aplicacion.ordentrabajovalor.OrdenTrabajoValor;
import py.com.base.aplicacion.ordentrabajovalor.OrdenTrabajoValorDAO;



/**
 *
 * @author hugo
 */

public class OrdenTrabajoExt extends OrdenTrabajo{
    
    
    private List<OrdenTrabajoDetalle> detalles = new ArrayList <OrdenTrabajoDetalle>() ;   
    private List<OrdenTrabajoValor> valores = new ArrayList <OrdenTrabajoValor>() ;  
    
    private Map<String, Long> suma = new HashMap<>();
     

    public void extender() throws IOException, Exception {

        
        
        OrdenTrabajoDetalleDAO detallesDAO = new OrdenTrabajoDetalleDAO();
        this.detalles = detallesDAO.lista(this.getOrden_trabajo());
                
        
        OrdenTrabajoValorDAO valoresDAO = new OrdenTrabajoValorDAO();
        this.valores = valoresDAO.lista(this.getOrden_trabajo());
                
        
        long suma_total_iva0 = 0L;
        long suma_total_iva5 = 0L;
        long suma_total_iva10 = 0L;
        long suma_total = 0L;
        
        
        for (Iterator<OrdenTrabajoDetalle> iterator = this.detalles.iterator(); iterator.hasNext();) {
            OrdenTrabajoDetalle det = iterator.next();
            
            suma_total_iva0 = suma_total_iva0 + det.getPorcentaje0();
            suma_total_iva5 = suma_total_iva5 + det.getPorcentaje5();
            suma_total_iva10 = suma_total_iva10 + det.getPorcentaje10();
            suma_total = suma_total + det.getSub_total();
            
        }
        
        
        this.suma.put("total_iva0", suma_total_iva0);
        this.suma.put("total_iva5", suma_total_iva5);
        this.suma.put("total_iva10", suma_total_iva10);        
        this.suma.put("total_factura", suma_total);
        
        
        
    }


    
     
            
}
