/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordentrabajo;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;
import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.util.Datetime;
import py.com.base.aplicacion.cliente.Cliente;
import py.com.base.aplicacion.ordentrabajodetalle.OrdenTrabajoDetalleDAO;
import py.com.base.aplicacion.ordentrabajovalor.OrdenTrabajoValorDAO;


/**
 *
 * @author hugom_000
 */

public class OrdenTrabajoDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public OrdenTrabajoDAO ( ) throws IOException  {
    }
      
    
    
    
    public List<OrdenTrabajo>  list (Integer page, Integer factura) {
                
        List<OrdenTrabajo>  lista = null;        
        try {                        
                        
            OrdenTrabajoRS rs = new OrdenTrabajoRS();            
            
            lista = new Coleccion<OrdenTrabajo>().resultsetToList(
                    new OrdenTrabajo(),
                    rs.list(page, factura)                    
            );                      
            
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    
    
    
    
    public List<OrdenTrabajo>  search (Integer page, String busqueda) {
                
        List<OrdenTrabajo>  lista = null;
        
        try {                       
                        
            OrdenTrabajoRS rs = new OrdenTrabajoRS();
            lista = new Coleccion<OrdenTrabajo>().resultsetToList(
                    new OrdenTrabajo(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    
          
    
    
    
    public OrdenTrabajo add (String json) throws Exception{
    
        OrdenTrabajo ordentrabajo = new OrdenTrabajo();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();   
        
        
        JsonObject jsonObject = gson.fromJson(json, JsonObject.class);    
        
        
        
        ordentrabajo.setFecha_recepcion(
                Datetime.castDate( jsonObject.get("fecha_recepcion").getAsString()));
        ordentrabajo.setFecha_entrega(
                Datetime.castDate( jsonObject.get("fecha_entrega").getAsString()));    
        Cliente cliente = new Cliente();
        cliente.setCliente(jsonObject.get("cliente").getAsInt());
        ordentrabajo.setCliente(cliente);
        
        
        ordentrabajo.setOff_set( jsonObject.get("off_set").getAsBoolean() );
        ordentrabajo.setTipografica(jsonObject.get("tipografica").getAsBoolean() );
                        
        ordentrabajo.setFactura(0);
        
        ordentrabajo = (OrdenTrabajo) persistencia.insert(ordentrabajo);      
        
        
        
        
        // detalles
        new OrdenTrabajoDetalleDAO().add_lote(jsonObject, ordentrabajo.getOrden_trabajo() );
        
        // valores
        new OrdenTrabajoValorDAO().add_lote(jsonObject, ordentrabajo.getOrden_trabajo() );
        
        
        return ordentrabajo ;
    }
 
    
    
    
    
    
    public OrdenTrabajoExt filtrarSinFactura ( Object objeto, Integer codigoID ) throws Exception {
        
        OrdenTrabajoExt returnObjecto = null ;
        Conexion conexion = new Conexion();
        conexion.conectar();            
        Statement  statement ;
        ResultSet resultset;

        
        String sql = 
                " SELECT *\n" +
                " FROM public.ordenes_trabajo\n" +
                " where orden_trabajo = " + codigoID +
                " and factura = 0  " ;
                        
        try
        {            
            statement = conexion.getConexion().createStatement();                           
            resultset = statement.executeQuery( sql);     

                        
            if(resultset.next()) 
            {   
                returnObjecto = (OrdenTrabajoExt) this.persistencia.extraerRegistro(resultset, objeto);
            }

                        
            resultset.close();
            conexion.desconectar(); 

        }
        catch (SQLException ex)
        {
            System.out.println(ex.getErrorCode());
            System.out.println(ex.getMessage());
        }    
                
        conexion.desconectar();               
        return returnObjecto;
        
    }        
    
        
    public Integer updateFactura ( Integer factura, Integer orden  ) throws SQLException  {
    
        Conexion conexion = new Conexion();
        conexion.conectar();            
        Statement  statement ;
        ResultSet resultset;
        
        
        String sql = 
                "   UPDATE ordenes_trabajo\n" +
                "   SET factura= " + factura + " \n" +
                "   WHERE orden_trabajo =  " + orden ;        
        
        
        statement = conexion.getConexion().createStatement();                           
        int rowsAffected = statement.executeUpdate(sql);     
        
                
        return rowsAffected;
        
    }        
    
}
