/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.ordentrabajo;

import java.util.Date;
import py.com.base.aplicacion.cliente.Cliente;

/**
 * @author hugo
 */


public class OrdenTrabajo {
    
    private Integer orden_trabajo;
    private Date fecha_recepcion;
    private Date fecha_entrega;
    private Cliente cliente;
    private Boolean off_set;
    private Boolean tipografica;
    private Integer factura;

    public Integer getOrden_trabajo() {
        return orden_trabajo;
    }

    public void setOrden_trabajo(Integer orden_trabajo) {
        this.orden_trabajo = orden_trabajo;
    }

    public Date getFecha_recepcion() {
        return fecha_recepcion;
    }

    public void setFecha_recepcion(Date fecha_recepcion) {
        this.fecha_recepcion = fecha_recepcion;
    }

    public Date getFecha_entrega() {
        return fecha_entrega;
    }

    public void setFecha_entrega(Date fecha_entrega) {
        this.fecha_entrega = fecha_entrega;
    }




    public Integer getFactura() {
        return factura;
    }

    public void setFactura(Integer factura) {
        this.factura = factura;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Boolean getOff_set() {
        return off_set;
    }

    public void setOff_set(Boolean off_set) {
        this.off_set = off_set;
    }

    public Boolean getTipografica() {
        return tipografica;
    }

    public void setTipografica(Boolean tipografica) {
        this.tipografica = tipografica;
    }
    
    
}



