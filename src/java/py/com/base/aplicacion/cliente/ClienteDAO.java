/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.base.aplicacion.cliente;

import java.io.IOException;
import java.util.List;
import nebuleuse.ORM.db.Coleccion;
import nebuleuse.ORM.db.Persistencia;


/**
 *
 * @author hugom_000
 */

public class ClienteDAO  {

    public Integer total_registros = 0;    
    private Persistencia persistencia = new Persistencia();      
    
    
    public ClienteDAO ( ) throws IOException  {
    }
      
    
    public List<Cliente>  list (Integer page) {
                
        List<Cliente>  lista = null;        
        try {                        
                        
            ClienteRS rs = new ClienteRS();            
            
            lista = new Coleccion<Cliente>().resultsetToList(
                    new Cliente(),
                    rs.list(page)                    
            );                      
            
            this.total_registros = rs.total_registros  ;
            
        }         
        catch (Exception ex) {                        
            System.out.println(ex.getMessage());
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }      
      

    
    
    
    
    
    
    public List<Cliente>  search (Integer page, String busqueda) {
                
        List<Cliente>  lista = null;
        
        try {                       
                        
            ClienteRS rs = new ClienteRS();
            lista = new Coleccion<Cliente>().resultsetToList(
                    new Cliente(),
                    rs.search(page, busqueda)
            );            
            
            this.total_registros = rs.total_registros  ;            
        }         
        catch (Exception ex) {                        
            throw new Exception(ex);
        }
        finally
        {
            return lista ;          
        }
    }          
    
          
 
    
    
}
